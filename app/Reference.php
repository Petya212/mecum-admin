<?php

namespace App;

use Petervig\LaravelAdmin\Model;

class Reference extends Model
{
    protected $guarded = [];
    public function image() {
        return $this->belongsTo('Petervig\LaravelAdmin\Image');
    }
}
