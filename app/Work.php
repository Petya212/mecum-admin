<?php

namespace App;

use Petervig\LaravelAdmin\Model;

class Work extends Model
{
    protected $guarded = [];
    public function image() {
        return $this->belongsTo('Petervig\LaravelAdmin\Image');
    }
    public function gallery() {
        return $this->morphToMany('Petervig\LaravelAdmin\Image', 'image_field', 'image_field')->wherePivot('image_field_relation', 'gallery')->withPivot('order')->orderBy('order', 'asc');
    }


}
