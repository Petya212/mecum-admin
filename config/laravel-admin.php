<?php

return [
    'models' => [
        [
            'model' => 'Petervig\LaravelAdmin\Image',
            'name' => 'images',
            'template' => 'images',
            'fields' => [
                'caption' => [
                    'default' => '{}',
                    'type' => 'html'
                ],
            ],
            'relations' => [
                'file' => [
                    'model_name' => 'files',
                    'type' => 'hasOne'
                ]
            ],
            'exclude_from_admin_pages' => true,
            'display_name_field' => 'file.name',
            'linkable' => false,
            'captions' => true
        ],
        [
            'model' => 'App\Reference',
            'name' => 'references',
            'fields' => [
                'title' => [
                    'default' => 'New Reference'
                ]
            ],
            'relations' => [
                'image' => [
                    'model_name' => 'images',
                    'type' => 'hasOne',
                    'selector_type' => 'images'
                ]
            ],
            'display_name_field' => 'title',
            'order_field' => 'order'
        ],
        [
            'model' => 'App\Language',
            'name' => 'languages',
            'fields' => [
                'language' => [
                    'default' => 'XX',
                ],
                'data' => [
                    'type' => 'json',
                    'default' => json_encode([
                        "seo" => [
                            "title" => "Mecum Stúdió",
                            "description" => ""
                        ],
                        "menu" => [
                            "about" => "rólunk",
                            "works" => "munkáink",
                            "services" => "szolgáltatásaink",
                            "contact" => "kapcsolat"
                        ],
                        "sections" => [
                            "about" => [
                                "title" => "Ötletelünk, tervezünk, megvalósítunk",
                                "text" => "25 évvel ezelőtt egy elkötelezett családapa saját vállalkozásba fogott és megalapította a MECUM Grafikai Stúdiót.  A vállalkozás rövid idő alatt megsokszorozta kapacitását: az elmúlt évtizedek során kicsik és nagyok egyaránt megtisztelték  bizalmukkal, amiért cserébe a kreatív ötleteket versenyképesen megvalósító, hosszútávú koncepciókban gondolkozó partnerre leltek. Azóta a gyermekek felnőttek, ám a családi vállalkozás szelleme megmaradt: a legidősebb lány vezetésével továbbra is a költség- és időhatékonyság szem előtt tartása maradt az alapelv, az esztétikum és az ügyfél kívánságainak tiszteletben tartása mellett. Célunk ma is az, mint 25 évvel ezelőtt volt: hogy munkánkkal maximálisan hozzájáruljunk ügyfeleink sikeréhez!"
                            ],
                            "works" => [
                                "title" => "Munkáink"
                            ],
                            "services" => [
                                "title" => "Mivel foglalkozunk?",
                                "brand" => [
                                    "title" => "Arculattervezés, Branding",
                                    "text" => "A profi vizuális megjelenés a jól megtervezett logóval és a tudatosan felépített arculattal kezdődik. A koncepciózusan felépített branding pedig könnyen azonosíthatóvá és vonzóvá teszi vállalkozásodat a piac hatalmas reklámzajában."
                                ],
                                "web" => [
                                    "title" => "Webtervezés, honlapépítés",
                                    "text" => "Az online térben 10-15 másodperc áll rendelkezésünkre, ennyi idő van arra hogy a látogatódat meggyőzzük, a Te oldaladon minden, számára fontos információt megtalál: nemcsak szép de átlátható, kezelhető formában is."
                                ],
                                "print" => [
                                    "title" => "Grafikai tervezés, Kivitelezés",
                                    "text" => "Az online jelenlét mellett fontos pillér a papíralapú megjelenés. Az így közvetített információ személyesebb, nagyobb jelentőséget tulajdonítunk neki. A nyomtatott anyag értéke a személyesség és a kézzelfoghatóság."
                                ],
                                "video" => [
                                    "title" => "Filmkészítés, Kreatív fotózás",
                                    "text" => "Kápráztasd el partnereidet egy hangulatos és informatív cég- vagy termékbemutató videóval! Vagy teremts olyan hangulatot modelljeid vagy a branded köré, ami garantáltan emlékezetessé teszi marketing anyagaidat."
                                ]
                            ],
                            "references" => [
                                "title" => "Referenciáink"
                            ],
                            "contact" => [
                                "title" => "Dolgozzunk együtt!",
                                "text" => "Lépj kapcsolatba velünk!",
                                "button" => "Árajánlat kérés",
                                "email_address" => "agnes.mozer@mecum.hu"
                            ]
                        ]
                    ]),
                ]
            ],
            'display_name_field' => 'language',
            'order_field' => 'order'
        ],
        [
            'model' => 'App\Work',
            'name' => 'works',
            'fields' => [
                'title' => [
                    'default' => 'New Work'
                ],
                'client' => [
                    'default' => ''
                ]
            ],
            'relations' => [
                'image' => [
                    'model_name' => 'images',
                    'type' => 'hasOne',
                    'selector_type' => 'images'
                ],
                'gallery' => [
                    'model_name' => 'images',
                    'type' => 'hasMany',
                    'selector_type' => 'images',
                    'order_field' => 'order'
                ]
            ],
            'display_name_field' => 'title',
            'order_field' => 'order'
        ]
    ],

    'file_storage_folder' => 'storage'
];