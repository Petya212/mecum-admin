import 'font-awesome/scss/font-awesome.scss';
import 'laravel-admin/src/resources/app.scss';
import LaravelAdmin from 'laravel-admin/src/resources/LaravelAdmin';
import Vue from 'vue';

import Toasted from 'vue-toasted';
Vue.use(Toasted, {iconPack: 'fontawesome'});

let components = LaravelAdmin.components;

for (var name in components) {
    Vue.component(name, components[name]);
}

new Vue({
    el: '#app',
   // components
});